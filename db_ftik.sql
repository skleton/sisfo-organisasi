-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2020 at 12:15 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ftik`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_controls`
--

CREATE TABLE `access_controls` (
  `id` int(11) NOT NULL,
  `role_is` int(11) DEFAULT NULL,
  `article` int(11) DEFAULT NULL,
  `news` int(11) DEFAULT NULL,
  `popup` int(11) DEFAULT NULL,
  `acl` int(11) DEFAULT NULL,
  `users_management` int(11) DEFAULT NULL,
  `app_config` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `actived`
--

CREATE TABLE `actived` (
  `id` int(11) NOT NULL,
  `description` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actived`
--

INSERT INTO `actived` (`id`, `description`) VALUES
(1, 'active'),
(2, 'deactive');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `deskripsion` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `posting_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `id` int(11) NOT NULL,
  `role_is` int(11) DEFAULT NULL,
  `id_income` int(11) DEFAULT NULL,
  `spending` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow_month`
--

CREATE TABLE `cashflow_month` (
  `id` int(11) NOT NULL,
  `role_is` int(11) DEFAULT NULL,
  `frist_income` bigint(20) DEFAULT NULL,
  `id_month` int(11) DEFAULT NULL,
  `income_month` bigint(20) DEFAULT NULL,
  `deskripsion` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `deskripsion` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `posting_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_album`
--

CREATE TABLE `gallery_album` (
  `id` int(11) NOT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int(11) NOT NULL,
  `role_is` int(11) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  `month_id` int(11) DEFAULT NULL,
  `income` bigint(20) DEFAULT NULL,
  `income_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `id` bigint(20) NOT NULL,
  `month` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `deskripsion` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `posting_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `is_role` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `is_role`, `category`, `created_at`) VALUES
(1, 1, 'superadmin', '2020-03-29 16:42:40'),
(2, 2, 'admin', '2020-03-29 16:42:41'),
(3, 3, 'member', '2020-03-29 16:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nim` varchar(150) DEFAULT NULL,
  `is_users` int(11) DEFAULT NULL,
  `actived` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nim`, `is_users`, `actived`, `role_id`, `name`, `email`, `password`, `telp`, `image`, `created_at`) VALUES
(1, '153124720650020', 1, 1, 1, 'Andika Pratama', 'akptma@gmail.com', '$2y$10$UXaz3E5LSbHkN3hKd97SkevrozMNIHM5igfaC2fqlI6Q5kpZSi.Gu', '089643771074', NULL, '2020-04-03 18:02:34'),
(2, '153124720650021', NULL, 2, 2, 'ploysornarinn', 'ploysornarinn@gmail.com', NULL, NULL, NULL, '2020-04-03 18:02:40'),
(3, '153124720650022', NULL, 1, 3, 'pramness', 'pramness@gmail.com', NULL, NULL, NULL, '2020-04-03 18:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(11) NOT NULL,
  `is_role` int(11) NOT NULL,
  `user_gent` varchar(125) NOT NULL,
  `remote_addr` varchar(125) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`id`, `is_role`, `user_gent`, `remote_addr`, `created_at`) VALUES
(1, 0, 'Chrome', '::1', '2020-03-29 07:24:18'),
(2, 1, 'Chrome', '::1', '2020-03-29 07:28:52'),
(3, 1, 'Chrome', '::1', '2020-03-29 08:17:03'),
(4, 1, 'Chrome', '::1', '2020-03-29 08:17:23'),
(5, 1, 'Chrome', '::1', '2020-03-29 08:23:31'),
(6, 1, 'Chrome', '::1', '2020-03-29 09:20:57'),
(7, 1, 'Chrome', '::1', '2020-03-29 09:39:24'),
(8, 1, 'Chrome', '::1', '2020-03-29 10:52:57'),
(9, 1, 'Chrome', '::1', '2020-03-29 11:30:56'),
(10, 1, 'Chrome', '::1', '2020-03-29 12:16:45'),
(11, 1, 'Chrome', '::1', '2020-03-29 16:28:01'),
(12, 1, 'Chrome', '::1', '2020-03-29 18:12:58'),
(13, 1, 'Chrome', '::1', '2020-04-01 14:58:21'),
(14, 1, 'Chrome', '::1', '2020-04-01 16:03:46'),
(15, 1, 'Chrome', '::1', '2020-04-02 13:23:51'),
(16, 1, 'Chrome', '::1', '2020-04-02 15:57:38'),
(17, 1, 'Chrome', '::1', '2020-04-03 14:04:20'),
(18, 1, 'Chrome', '::1', '2020-04-03 16:08:24'),
(19, 1, 'Chrome', '::1', '2020-04-03 17:06:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_controls`
--
ALTER TABLE `access_controls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `actived`
--
ALTER TABLE `actived`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashflow_month`
--
ALTER TABLE `cashflow_month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_album`
--
ALTER TABLE `gallery_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_controls`
--
ALTER TABLE `access_controls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `actived`
--
ALTER TABLE `actived`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashflow_month`
--
ALTER TABLE `cashflow_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery_album`
--
ALTER TABLE `gallery_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
