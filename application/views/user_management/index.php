    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Anggota Organisasi</h6>
            <!-- <p class="h3 mb-0 text-gray-800">Dashboard</p> -->
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>
        </div>

        <div class="card-body" >
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" style="font-size:13px; text-align: justify;" >
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Position</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody id="showdata">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php echo $encrypt?>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){

                 $('#dataTable').DataTable({ 
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, 
                    // "orderable" :true,

                    
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('UsersManagement/get_ajax'); ?>",
                        "type": "POST"
                    },
                    "columnDefs" : [
                        {
                            "targets" : [0, 3, -1],
                            "orderable" : false
                        }
                    ]
                });
    });
    </script>