<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersManagement extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('UsersManagement_model', 'UsersManage');
        $this->load->library('datatables');
    }

    public function index () {
        if ($this->session->userdata('role') == 1) {
            //SUPERADMIN
            //    $data['get_all'] = $this->UsersManage->show();
               $data['title'] = 'User-Manage';
               $this->load->view('layouts/head', $data);
               $this->load->view('user_management/index');
               $this->load->view('layouts/footer');
    
           }elseif ($this->session->userdata('role') == 2) {
               //ADMIN
           }elseif ($this->session->userdata('role') == 3) {
            // MEMBER
           }else {
            redirect('auth');
        }
    }//END OF PUBLIC FUNCTION INDEX
    function get_ajax() {
        $list = $this->UsersManage->get_datatables();
        // dump_exit($list);
        $data = array();
        // dump_exit($data);
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no.".";
            
            // $row[] = $item->barcode.'<br><a href="'.site_url('item/barcode_qrcode/'.$item->item_id).'" class="btn btn-default btn-xs">Generate <i class="fa fa-barcode"></i></a>';
            $row[] = $item->nim;
            $row[] = strtoupper($item->name);

            if ($item->description =='active') {

                $row[] = '<span class="badge badge-info">'. strtoupper($item->description). '</span>';
                
            }else{
                
                $row[] = '<span class="badge badge-dark">'. strtoupper($item->description). '</span>';
            }

            if ($item->category =='superadmin') {

                $row[] = '<span class="badge badge-danger">'. strtoupper($item->category). '</span>';

            }elseif ($item->category =='admin') {
                
                $row[] = '<span class="badge badge-warning">'. strtoupper($item->category). '</span>';

            }else{

                $row[] = '<span class="badge badge-primary">'. strtoupper($item->category). '</span>';
            }

            $row[] = '<a href="'.base_url('UsersManagement/get_ajax/'.$item->encrypt).'" class="badge badge-info" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i> Update</a>';
            
            $data[] = $row;
        }

        // dump_exit($data[0][4]);
        
        $output = array(
                    "draw" => @$_POST['draw'],
                    "recordsTotal" => $this->UsersManage->count_all(),
                    "recordsFiltered" => $this->UsersManage->count_filtered(), 
                    "data" => $data,
                );
        // output to json format
        echo json_encode($output);
    }

    public function detailData($encrypt){
        if ($encrypt != NULL) {
            echo $encrypt;
            // $get = $this->db->get_where('users', ['id'=>$id])->row();
            // dump_exit($get);
        }else{
            echo blockmethod;
        }
    }


}//END OF CLASS